﻿using System;
namespace TemperatureController.Properties
{
    public class ContentData
    {
        public int deviceID { get; set; } //BOF
        public string connectionString { get; set; }  // fetch from BOF
        public string lng { get; set; } // fetch from vcom
        public string height { get; set; } // fetch from vcom
        public string speed { get; set; } // fetch from vcom
        public string orient { get; set; } // fetch from vcom
        public string engine_status { get; set; } // fetch from vcom
        public string accuracy { get; set; } // fetch from vcom
        public string acceleration_x { get; set; } // fetch from vcom
        public string acceleration_y { get; set; } // fetch from vcom
        public string acceleration_z { get; set; } // fetch from vcom
        public string event_time { get; set; } // fetch from vcom


        // connectionString example=>  HostName=IOTHUB-01102021.azure-devices.net;DeviceId=["deviceID"];SharedAccessKey=["Symmetric key"]
    }
}